import React from 'react';
import PropTypes from 'prop-types';

const Clima = ({resultado}) => {
    //extraemos los datos

    const {name,main}=resultado;
    const kelvin = 273.15;
    // !name quiere decir si no tiene name... se le coloca para que cuando cargue no de error al no traer nada.
    if(!name) return null;
    return ( 
        <div className="card-panel white col s12">
            <div className="black-text">
                <h2>{name}:</h2> 
                <p className="temperatura">
                    {parseFloat(main.temp - kelvin,10).toFixed(2)}<span> &#x2103; </span>
                </p>
                <p>Sensación térmica: {parseFloat(main.feels_like - kelvin,10).toFixed(2)}<span> &#x2103; </span>
                </p>
                <p>Maxima: {parseFloat(main.temp_max - kelvin,10).toFixed(2)}<span> &#x2103; </span>
                </p>
                <p>Mimina: {parseFloat(main.temp_min - kelvin,10).toFixed(2)}<span> &#x2103; </span>
                </p>
            </div>
        </div>
     );
}
 Clima.propTypes={
     resultado: PropTypes.object.isRequired
 }
export default Clima;