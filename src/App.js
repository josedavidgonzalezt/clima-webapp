import React, {Fragment,useState,useEffect} from 'react';
import Header from './components/Header';
import Formulario from './components/Formulario';
import Clima from './components/Clima';
import Error from './components/Error';


function App() {

  // State del formulario
  const [busqueda, guardarBusqueda]=useState({
    ciudad:'',
    pais:''
  });
  //extraer
  const {ciudad,pais}= busqueda;
  //estate de consulta
  const [consultar, guardarConsultar]=useState(false);
  const [resultado,guardarResultado]= useState({});
  const [error,guardarError]=useState(false);

  useEffect(() => {
    const consultarAPI= async ()=>{
      //si lo ejecuto con el if, se hace automaticamente la consulta a la appi al cargar la pagina, pero con el if, se consulta despues darle consultar
      if(consultar){
        const appId= 'b78fadad2ff0424e0be7e327b23a66a8';
        const url =`https://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${appId}`;
        const respuesta= await fetch(url);
        const resultado = await respuesta.json();
        //Le pasamos la consulta
        guardarResultado(resultado);
        guardarConsultar(false);
        // detecta si hubo resultados correctos
        if(resultado.cod==="404"){
          guardarError(true);
        }
        else {
          guardarError(false);
        }
      } 
    }
    consultarAPI();
    /*la linea de abajo la colocamos para que obvie las dependencias de ciudad y pais la variable importante en este caso es consultar*/
    //eslint-disable-next-line
  }, [consultar])
  const sinResultado =()=>{
    return(
      <div>
        <h6>No hay resultados</h6>
        <div>Intente sin acentuaciones u otro nombre</div>
      </div>
    );
  }
  let componente;
  if (error){
    componente= <Error mensaje={sinResultado()}/>
  }
  else{
    componente = <Clima
                    resultado={resultado}
                 />
  }

  return (
    <Fragment>
      <Header
        titulo='CLIMA'
      />
      <div className="contenedor-form">
          <div className="container">
            <div className="row">
              <div className="col m6 s12 clima-form">
                <Formulario
                  busqueda={busqueda}
                  guardarBusqueda={guardarBusqueda}
                  guardarConsultar={guardarConsultar}
                />
              </div>
              <div className="col m6 s12">
                {componente}
              </div>
            </div>
          </div>
      </div>
    </Fragment>
  );
}

export default App;
